# SDN LLDP Relay attack

## Getting started

You need previously to have:
- ONOS Controller (tested it!) or other SDN controller
- Mininet 2.2+

## Run

### Start sdn controller and mininet
- Starting ONOS (SDN controller)
- Run mininet ( <code>sudo mn --custom topos/host-relay-topo.py --topo mytopo --mac --controller remote --switch=ovsk,protocols=OpenFlow13</code> )

### Host Relay
- From the mininet CLI, run xterm hR to open the RELAY HOST terminal
- Navigate to sdn_lldp_relay/ (this repository)
- From hR's xterm, run <code>sudo scripts/relay.sh</code>
- From the mininet controller, run pingall to give all hosts an IP address
