"""Partial-Mesh topology

Five directly connected switches plus a host for each access switch. 

Adding the 'topos' dict with a key/value pair to generate our newly defined
topology enables one to pass in '--topo=mytopo' from the command line.
"""

from mininet.topo import Topo
from mininet.link import TCLink

class MyTopo( Topo ):
    "Partial-mesh topology."

    def build( self ):
        "Create custom topo."

        # Add hosts and switches
        h1 = self.addHost( 'h1' )
        h2 = self.addHost( 'h2' ) 
        ## core
        s1 = self.addSwitch( 's1' )
        ## aggregation
        s2 = self.addSwitch( 's2' )
        s3 = self.addSwitch( 's3' )
        ## access
        s4 = self.addSwitch( 's4' )
        s5 = self.addSwitch( 's5' )

        # Add links
        ## aggregation - core links
        self.addLink( s2, s1, cls=TCLink, bw=10 )
        self.addLink( s3, s1, cls=TCLink, bw=10 )

        ## access - aggregation links
        ### left switches
        self.addLink( s4, s2, cls=TCLink, bw=10 )
        self.addLink( s4, s2, cls=TCLink, bw=10 )
        ### right switches
        self.addLink( s5, s3, cls=TCLink, bw=10 )
        self.addLink( s5, s3, cls=TCLink, bw=10 )
        ### redundant
        self.addLink( s4, s3, cls=TCLink, bw=10 )
        self.addLink( s5, s2, cls=TCLink, bw=10 )
        ## hosts links
        self.addLink( h1, s4 )
        self.addLink( h2, s5 )

        # LLDP Relay Attack 
        
        ## Host attacker
        relay_host = self.addHost( 'hR' )
        ## Attacker links
        self.addLink( relay_host, s4 )
        self.addLink( relay_host, s5 )


topos = { 'mytopo': ( lambda: MyTopo() ) }
