#!/bin/bash

# disable host Relay's links with s4 (leaf1) and s5 (leaf2)
ifconfig hR-eth0 0.0.0.0 down
ifconfig hR-eth1 0.0.0.0 down

# set up a network bridge
brctl addbr hR-br0
brctl addif hR-br0 hR-eth0
brctl addif hR-br0 hR-eth1

# allow lldp forwarding
echo 16384 > /sys/class/net/hR-br0/bridge/group_fwd_mask

# re-connect to switches using invisible link
ifconfig hR-eth0 up
ifconfig hR-eth1 up
ifconfig hR-br0 up
